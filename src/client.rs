use std::fs::File;
use std::io::{Read, Write};

fn main() {
    // Since we're not making a distinction between creating a new file,
    // opening an existing file, read or write modes in our scheme's open
    // method, it doesn't matter whether we use File::open or File::create.
    // It also doesn't matter what path components exist after the colon,
    // since our scheme's open function doesn't change it's behavior based
    // on which path was passed to it.
    let mut vec_file = File::open("vec:/hi")
        .expect("Failed to open vec file");

    vec_file.write(b" Hello")
        .expect("Failed to write to vec:");

    let mut read_into = String::new();
    vec_file.read_to_string(&mut read_into)
        .expect("Failed to read from vec:");

    println!("{}", read_into); // olleH ih/
}

